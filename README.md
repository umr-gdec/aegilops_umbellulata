# TE annotation

Transposable elements of the *Aegilops umbellulata* genome have been annotated thanks to the CLARI-TE_smk pipeline developped at GDEC research unit, INRAE.  
This project was carried out in collaboration with Gill Upinder (North Dakota State University).  
from /home/palasser/projects/Aegilops_umbellulata on HPC2 cluster (Mésocentre, Université Clermont-Auvergne, France)

## Material & Methods  

Material: *Aegilops umbellulata* fasta file  
Methods: CLARI-TE_smk (pipeline developped at INRAE), R  

## CLARI-TE_smk source and download  

To access TEs annotation pipeline gitlab repository: [CLARI-TE_smk](https://forgemia.inra.fr/umr-gdec/clari-te_smk.git)  

To download the pipeline:  

```console
git clone https://forgemia.inra.fr/umr-gdec/clari-te_smk.git
```

Download the Singularity image:  

```console
singularity pull oras://registry.forgemia.inra.fr/umr-gdec/clari-te_smk:latest
```

## Results  

The TEs content has been described, and TEs length proportion changes of TE-families have been compared to reference sub-genomes.  

![Table](table.png)  

## Support  

pauline.lasserre-zuber@inrae.fr  
Bioinformatic Ingineer  
pauline.lasserre-zuber@inrae.fr  
UMR INRAE UCA 1095 [GDEC](https://umr1095.clermont.hub.inrae.fr/)  

## Authors and acknowledgment  

Pauline LASSERRE-ZUBER (INRAe), Frederic CHOULET (INRAe)  

## Reference  

[Singh et al, 2024](https://www.biorxiv.org/content/10.1101/2024.01.13.575480v1)  